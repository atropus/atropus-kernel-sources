atropus-kernel-sources
======================

This repository hosts the kernel sources I use to build my own custom kernel in Gentoo.

Sumary : Vanilla linux kernel tweaked with various patches to improve laptop/desktop performance.
